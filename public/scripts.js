function toggleVisibility() {
    hideAll();
    showClicked(event);
}

function hideAll() {
    let elements = document.querySelectorAll(".sidenav ul li ul");
    elements.forEach(el => {
        el.classList.add("hidden")
    });
}

function showClicked(event) {
    let el = event.target.parentNode.querySelector("ul");
    el.classList.remove("hidden");
}

function generateSideMenu() {
    const sideNav = document.getElementById("sidenav-list");
    const headers = document.querySelectorAll("h2, h3");

    let currentSublist = undefined;
    headers.forEach(h => {
        if (h.nodeName.toLowerCase() === "h2") {
            const li = createListItem(h, toggleVisibility);
            currentSublist = createEmptyUnorderedList()
            li.appendChild(currentSublist);
            sideNav.appendChild(li);
        } else if (h.nodeName.toLowerCase() === "h3") {
            currentSublist.appendChild(createListItem(h));
        }
    });
}

function createListItem(h, onclickFunction) {
    const li = document.createElement("li");
    const a = createLinkFromHeader(h);

    if (onclickFunction) {
        a.onclick = onclickFunction;
    }

    li.appendChild(a);
    return li;
}

function createEmptyUnorderedList() {
    const ul = document.createElement("ul");
    ul.classList.add("hidden", "small-font");
    return ul;
}

function createLinkFromHeader(h) {
    const a = document.createElement("a");
    a.innerText = h.innerText;
    a.href = "#" + h.id;
    return a;
}